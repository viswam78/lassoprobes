## Long-adapter single-strand oligonucleotide probes for the massively multiplexed cloning of kilobase genome regions

As the catalogue of sequenced genomes and metagenomes continues to grow, massively parallel approaches for the comprehensive and functional analysis of gene products and regulatory elements are becoming increasingly valuable. Current strategies to synthesize or clone complex libraries of DNA sequences are limited by the length of the DNA targets, throughput and cost. Here, we show that long-adapter single-strand oligonucleotide (LASSO) probes can capture and clone thousands of kilobase DNA fragments in a single reaction. As a proof-of-principle, we simultaneously cloned >3,000 bacterial open reading frames (ORFs) from E. coli genomic DNA (spanning 400–5,000 bp targets). Targets were enriched up to a median of ~60-fold compared to non-targeted genomic regions. At a cutoff of 3 times the median non-target reads per kilobase of genetic element per million reads, ~75% of the targeted ORFs were successfully captured. We also show that LASSO probes can clone human ORFs from complementary DNA, and an ORF library from a human-microbiome sample. LASSO probes could be used for the preparation of long-read sequencing libraries and for massively multiplexed cloning.

# Github repository with the original nBME paper
# Clone with additional datasets and a public repository here in bitbucket as well  

Input directory has the fasta and fastq files  

Scripts has 1. Snakefile for NGS analyses and 2. R markdown for deep analyzing  

coverageATGCfiles and regionIndex have intermediate files as a result of running Snakefile, but useful for R markdown

Author: VS (reach me at V.S@gMdotC)
Date: Feb 11, 2019
Place: Manasarovar

Regarding the publication: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5687285/


