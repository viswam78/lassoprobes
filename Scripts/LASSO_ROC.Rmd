---
Author: Viswanadham Sridhara
Date: 21 February 2017 (Re-edited in Feb 2019 to plot only ROC)
Project: Long Adapter Single Stranded Oligonucleotide (LASSO) Probes for Massively
  Multiplexed Cloning of Kilobase-Sized Genome Regions
Title: R markdown file using sample input files
output:
  html_document:
    df_print: paged
---
```{r, message = FALSE}
# loading all the required packages

rm(list=ls())

library(seqinr)
library(plyr)
library(dplyr)
library(cowplot)
library(ggplot2)
library(Rsamtools)
library(sfsmisc)
library(plotROC)
library(mygene)
```


```{r}

targetsData<-read.table("../sample_data/SRR3956929.target", header=FALSE)
nontargetsData<-read.table("../sample_data/SRR3956929.nontarget", header=FALSE)
complementData<-read.table("../sample_data/SRR3956929.complement", header=FALSE)
names(targetsData)<-c("Chromosome","Start","Stop","Gene",  "Strand", "Reads", "BasesCovered", "Length", "FractionCoverage")
names(nontargetsData)<-c("Chromosome","Start","Stop", "Gene", "Strand", "Reads", "BasesCovered", "Length", "FractionCoverage")
names(complementData)<-c("Chromosome","Start","Stop", "Gene", "Strand", "Reads", "BasesCovered", "Length", "FractionCoverage")

targetsDataDepth<-read.table("../sample_data/SRR3956929.d_target", header=FALSE)
nontargetsDataDepth<-read.table("../sample_data/SRR3956929.d_nontarget", header=FALSE)
complementDataDepth<-read.table("../sample_data/SRR3956929.d_complement", header=FALSE)
names(targetsDataDepth)<-c("Chromosome","Start","Stop","Gene",  "Strand" ,"Pos",  "Depth")
names(nontargetsDataDepth)<-c("Chromosome","Start","Stop", "Gene", "Strand" , "Pos", "Depth")
names(complementDataDepth)<-c("Chromosome","Start","Stop", "Gene", "Strand" , "Pos", "Depth")

# Read ATGC content, and also other features of the gene #
ATGC_content<-read.table("../coverageATGCfiles/targetsCoverageATGC.txt", header = TRUE)

non_ATGC_content<-read.table("../coverageATGCfiles/nontargetsCoverageATGC.txt", header = TRUE)

comp_ATGC_content<-read.table("../coverageATGCfiles/complementCoverageATGC.txt", header = TRUE)

names(ATGC_content) <- c("Chromosome", "Start", "Stop", "Gene", "Strand", "PCT_AT", "PCT_GC", "NUM_A", "NUM_C", "NUM_G", "NUM_T", "NUM_N", "NUM_OTHER", "Length")

names(non_ATGC_content) <- c("Chromosome", "Start", "Stop", "Gene", "Strand", "PCT_AT", "PCT_GC", "NUM_A", "NUM_C", "NUM_G", "NUM_T", "NUM_N", "NUM_OTHER", "Length")

names(comp_ATGC_content) <- c("Chromosome", "Start", "Stop", "Gene", "Strand", "PCT_AT", "PCT_GC", "NUM_A", "NUM_C", "NUM_G", "NUM_T", "NUM_N", "NUM_OTHER", "Length")

```

```{r}
head(targetsData)
```

```{r}
head(targetsDataDepth)
```

```{r}
head(ATGC_content)
```


```{r}
    
# Merge all the read files individually for targets, non-targets etc
targetsDataD<-merge(targetsData, targetsDataDepth, by=intersect(names(targetsData), names(targetsDataDepth)))

nontargetsDataD<-merge(nontargetsData, nontargetsDataDepth, by=intersect(names(nontargetsData), names(nontargetsDataDepth)))

complementDataD<-merge(complementData, complementDataDepth, by=intersect(names(complementData), names(complementDataDepth)))

    
targetsAllData<-merge(targetsDataD, ATGC_content, by=intersect(names(targetsDataD), names(ATGC_content)))

nontargetsAllData<-merge(nontargetsDataD, non_ATGC_content, by=intersect(names(nontargetsDataD), names(non_ATGC_content)))

complementAllData<-merge(complementDataD, comp_ATGC_content, by=intersect(names(complementDataD), names(comp_ATGC_content)))
    
## Reviewers requested to merge Non-target and intergenic and name them as non-targets or we can call them off-targets ##
targetsAllData$Region="Target"
nontargetsAllData$Region="Non-Target"
complementAllData$Region="Non-Target"


test<-rbind(targetsAllData,nontargetsAllData)
allData<-rbind(test,complementAllData)
levels(allData$Region)<-levels(allData$Region)[2:1]
allData$Region <- factor(allData$Region, levels = c("Target", "Non-Target"))

totalReads<-sum(allData$Reads)
allData$FPKM<-(1e9/totalReads)*(allData$Reads/allData$Length)

head(allData)

```

```{r}

filteredData <- subset(allData, (Length >= 400), select=c(Reads, Depth, Region, Length, FractionCoverage, FPKM))
filteredDataTNT <- subset(allData, (Length >= 400) & ( Region == "Target" | Region == "Non-Target"), select=c(Reads, Depth, Region, Length, FractionCoverage, FPKM))
filteredDataTNT$D<-NULL
filteredDataTNT$D[which(filteredDataTNT$Region=="Target")]<-1
filteredDataTNT$D[which(filteredDataTNT$Region=="Non-Target")]<-0
filteredDataTNT$M<-filteredDataTNT$Depth
head(filteredDataTNT)
```

```{r}

# ROC plot
ROCplot <- ggplot(filteredDataTNT, aes(d = D, m = M)) + geom_roc(n.cuts = 0, color = "blue")+
  labs(x="False Positive Rate", y = "True Positive Rate")+
  #+style_roc()
theme(axis.title.y = element_text(face="bold", colour="black", size=20),axis.text.y  = element_text(vjust=0.5, size=20),
axis.title.x =element_text(face="bold", colour="black", size=20),axis.text.x  = element_text(vjust=0.5, size=20))+
  scale_y_continuous(expand=c(0,0),breaks=c(0,.25,.5,.75,1),labels=c(0,.25,.5,.75,1))+
  scale_x_continuous(expand=c(0,0),breaks=c(.25,.5,.75,1),labels=c(.25,.5,.75,1))
ROCplot<-ROCplot+annotate("text", x = .6, y = .3, size=9, color="blue",
           label = paste("AUC =", round(calc_auc(ROCplot)$AUC, 3))) 
ROCplot
```




